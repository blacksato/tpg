@extends('layouts.app')
@section('titleOption',__('options.roles'))
@section('bar-search')
    @include('partials.search',[
      'route'=>route('roles.index'),
      'placeholder'=>__('labels.search',['name'=>__('labels.title')]),
      'filter'=>$filter
    ])
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                       

                        <div class="card-header">
                                <div class="card-actions">
                                   <a class="btn-minimize text-danger" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                                </div>
                                <h4 class="card-title">@lang('options.table',['name'=>__('options.roles')])</h4>
                                <div class="card-subtitle">
                                        @can ('create-role')
                                        <a href="{{route('roles.create')}}" class="btn btn-info btn-rounded waves-effect waves-light btn-sm text-white">
                                            @lang('labels.add')</a>
                                        @endcan    
                                </div>
                        </div>


                    <div class="card-body collapse show">
                        
                        
                        <table class="table product-overview table-responsive-stack">
                            <thead>
                                <tr>
                                    <th class="text-center">@lang('labels.name')</th>
                                    <th class="text-center"> @lang('labels.description') </th>
                                    <th class="text-center"> @lang('labels.actions')  </th>
                                </tr>
                            </thead>
                            <tbody>
                               @forelse($roles as $role)
                                <tr>
                                    <td class="text-center">{{$role->name}}</td>
                                    <td class="text-center">{{$role->title}}</td>
                                    <td class="text-center"> 
                                        <form  action="{{route('roles.destroy',$role->id)}}" method="POST" class="form-horizontal" @submit.prevent="deleteAlert($event)">
                                            @method('DELETE')
                                            @csrf
                                            @can('show-role')
                                                <a href="{{route('roles.show',$role->id)}}"><span class="btn waves-effect waves-light btn-xs btn-success text-uppercase">@lang('options.menu-ability')</span></a>
                                            @endcan
                                            @can('update-role')
                                                <a href="{{route('roles.edit',$role->id)}}"><span class="btn waves-effect waves-light btn-xs btn-info">@lang('labels.edit')</span></a>
                                            @endcan
                                            @can ('delete-role')
                                                <button type="submit" class="btn waves-effect waves-light btn-xs btn-danger"><span>@lang('labels.delete')</span></button> 
                                            @endcan
                                        </form>

                                    </td>
                                </tr>
                               @empty
                               <tr><td colspan="6">@lang('labels.notresults')</td></tr>
                               @endforelse
                            </tbody>
                        </table>
                        <div class="text-right">
                            {!! $roles->appends(['filter' => $filter])->render()!!}
                    </div>
                    </div>
                </div>

             </div>     
    </div>
</div>
@endsection