<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('partials.head')
</head>
<body class="fix-header fix-sidebar card-no-border">
    <div id="app">
        <main>
           <div class="preloader">
                <svg class="circular" viewBox="25 25 50 50">
                  <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> 
                </svg>
            </div> 
            <div id="main-wrapper">
                    <header class="topbar">
                            <nav class="navbar top-navbar navbar-expand-md navbar-light"> 
                               <div class="navbar-header">
                                    <a class="navbar-brand" href="{{route('home')}}">
                                        <b>
                                            <img src="{{asset('images/logo-icon.png')}}" alt="@lang('options.home')" class="dark-logo" />
                                            <img src="{{asset('images/logo-icon.png')}}" alt="@lang('options.home')" class="light-logo"  style="width: 34px;height: 33px"/>
                                        </b>
                                        <span>
                                            <img src="{{asset('images/logo-text.png')}}" alt="@lang('options.home')" class="dark-logo" />
                                            <img src="{{asset('images/logo-light-text.png')}}" class="light-logo" alt="@lang('options.home')" />
                                        </span> 
                                    </a>
                                </div>
                                <div class="navbar-collapse">
                                     <ul class="navbar-nav mr-auto mt-md-0">
                                        <!-- This is  -->
                                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                                         
                                        @yield('bar-search','')
                                        <li class="nav-item dropdown mega-dropdown"> <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="mdi mdi-view-grid"></i></a>
                                            <div class="dropdown-menu scale-up-left">
                                                <ul class="mega-dropdown-menu row">
                                                    <li class="col-lg-8 col-xlg-8 m-b-30">
                                                       @include('partials.slidesBanner')
                                                    </li>
                                                    
                                                    <li class="col-lg-4  m-b-30">
                                                        @admin
                                                        <contact-form-admin select="{{route('search-user')}}" route="{{route('sendNotificationAlert')}}"></contact-form-admin>
                                                        @else
                                                        <contact-form route="{{route('sendNotificationAlert')}}"></contact-form>
                                                        @endadmin
                                                       
                                                      
                                                    </li>
                                                   
                                                </ul>
                                            </div>
                                        </li>
                                       </ul>
                                    <ul class="navbar-nav my-lg-0">
                                    
                                        <notification route="{{route('searchNotification')}}"></notification>
                                      
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img onerror="this.onerror=null;this.src='{{asset('images/user.png')}}';" 
                                                src="{{currentUser()->avatar}}" alt="user" class="profile-pic" /></a>
                                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                                <ul class="dropdown-user">
                                                    <li>
                                                        <div class="dw-user-box">
                                                            <div class="u-img"><img  onerror="this.onerror=null;this.src='{{asset('images/user.png')}}';" 
                                                                src="{{currentUser()->avatar}}" alt="user"></div>
                                                            <div class="u-text">
                                                                <h4>{{currentUser()->name}}</h4>
                                                                <p class="text-muted">{{currentUser()->email}}</p><a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                                        </div>
                                                    </li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                                                    <li><a href="{{ route('changePicture') }}"><i class="fa fa-image"></i> @lang('labels.change-picture')</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="{{ route('changePassword') }}"><i class="ti-key"></i> @lang('labels.change-password')</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li>
                                                            <a href="#"  onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();"
                                                            class="link"><i class="fa fa-power-off"></i>&nbsp; @lang('Logout')</a>                                              
                                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            {{ csrf_field() }}
                                                            </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="flag-icon flag-icon-us"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right scale-up"> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-in"></i> India</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-fr"></i> French</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-cn"></i> China</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-de"></i> Dutch</a> </div>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                    </header>                
          
                    @include('partials.menu')
       
                    <div class="page-wrapper">
                             <div class="container-fluid">
                                <div class="row page-titles">
                                    <div class="col-md-5 col-8 align-self-center">
                                        <h3 class="text-themecolor">@yield('titleOption')</h3>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="{{route('home')}}">@lang('options.home')</a></li>
                                            <li class="breadcrumb-item active">@yield('titleOption')</li>
                                        </ol>
                                    </div>
                                </div>
                                <div class="row"> 
                                    @yield('content')
                                </div>
                            </div>
                    </div>
            </div>
            
        </main>
    </div>
    @include('partials.alertsFooter')
</body>
</html>