@if ($item['submenu'] == [])
    <li>
       <label>
            <input id="cbtest{{$item['id']}}"
            @if($item['check'] !=null) checked @endif 
            type="checkbox" name="menu[]" value="{{$item['id']}}">
            <label for="cbtest{{$item['id']}}" class="check-box"></label>&nbsp;{{ $item['name'] }}
        </label>
    </li>
@else
    <li >
            <label>
                    <input id="cbtest{{$item['id']}}" type="checkbox" 
                    @if($item['check'] !=null) checked @endif 
                    name="menu[]" value="{{$item['id']}}">
                    <label for="cbtest{{$item['id']}}" class="check-box"></label>&nbsp;{{ $item['name'] }}
                </label>
        <ul>
            @foreach ($item['submenu'] as $key=> $submenu)
                @if ($submenu['submenu'] == [])
                    <li>  <label>
                            <input id="check-unique-{{$submenu['id']}}" type="checkbox" 
                            @if($submenu['check'] !=null) checked @endif 
                            name="menu[]" value="{{$submenu['id']}}">
                            <label for="check-unique-{{$submenu['id']}}" class="check-box"></label>&nbsp;{{ $submenu['name'] }}
                        </label></li>
                @else
                    @include('partials.menu-item-tree', [ 'item' => $submenu ])
                @endif
            @endforeach
        </ul>
    </li>
@endif