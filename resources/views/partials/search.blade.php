<li class="nav-item hidden-sm-down search-box">
        <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
        <form class="app-search" method="GET" action="{{$route}}">
            @csrf
            <input type="text" name='filter' class="form-control" placeholder="{{$placeholder}}" value="{{$filter}}"> 
            <a class="srh-btn"><i class="ti-close"></i></a> 
        </form>
</li>