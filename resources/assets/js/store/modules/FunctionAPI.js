export default {
    actions:{
        sendPost({commit}, item){
            return axios.post(item.route,item.parameters);
        }      
    }
}