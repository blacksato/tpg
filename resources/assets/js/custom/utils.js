function showLoading() {
    $(".preloader").fadeIn();
}

function hideLoading() {
    $(".preloader").fadeOut();
}

function processErrorVue(context,_error) {
    hideLoading();
    if (_error.request!=undefined) {
        if(_error.request.status == 422){
            var errores = '';
            $.each(_error.response.data.errors, function (key, value) {
                errores += value[0] + "<br/>";
            });
            context.$message({
                type: 'error', dangerouslyUseHTMLString: true,
                message: '<b>Error de validación: </b><br/>' + errores,
                duration: 5000
            });
        }else{
            context.$message({
                type: 'error',
                message: 'Error: ' + _error.response.data.message
            });
        }
    } else {
        if(_error.response!=undefined){
            context.$message({
                type: 'error',
                message: 'Error: ' + _error.response.data.message
            });

        }else{
            context.$message({
                type: 'error',
                message: 'Error: ' + _error.message
            });
        }

    }

}