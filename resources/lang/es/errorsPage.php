<?php

return [
    '404' => [
        'title' => 'Página no encontrada!',
        'msg' => 'Regresar a la página principal',
    ],
    '403' => [
        'title' => 'Acceso no autorizado!',
        'msg' => 'Regresar a la página principal',
    ],
];
