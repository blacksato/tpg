<?php

Route::group(['middleware' => 'auth:web,admin'], function () {
    Route::post('send-notification-alert', 'NotificationsController@sendNotifications')->name('sendNotificationAlert');
    Route::post('search-notification-alert', 'NotificationsController@searchNotification')->name('searchNotification');
    Route::get('all-notification', 'NotificationsController@getAllNotifications')->name('getAllNotification');
    Route::get('notification-read-all', 'NotificationsController@readAllNotifications')->name('readAllNotification');
    Route::get('notification-check/{notification}', 'NotificationsController@check')->name('check-notification');
    Route::get('notification-uncheck/{notification}', 'NotificationsController@uncheck')->name('uncheck-notification');
    Route::get('notification/{notification}', 'NotificationsController@getNotification')->name('getNotification');
});
