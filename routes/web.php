<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();

Route::get('/master/{router?}', 'HomeController@getRouter')->name('master')->middleware('auth:web,admin');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth:web,admin');
Route::get('/cambiar-clave', 'HomeController@showChangePasswordForm')->name('changePassword')->middleware('auth:web,admin');
Route::post('/cambiar-clave', 'HomeController@changePassword')->name('changePassword')->middleware('auth:web,admin');
Route::get('/cambiar-foto', 'HomeController@showChangePictureForm')->name('changePicture')->middleware('auth:web,admin');
Route::post('/cambiar-foto', 'HomeController@changePicture')->name('changePicture')->middleware('auth:web,admin');



// Admin Login
Route::get('core-admin/login', 'Admin\LoginController@showLoginForm')->name('admin.login');
Route::post('core-admin/login', 'Admin\LoginController@login');
Route::post('core-admin/logout', 'Admin\LoginController@logout')->name('admin.logout');

require __DIR__ . '/modules/notifications.php';
require __DIR__ . '/modules/guest.php';

Route::view('/readme', 'readme')->name('readme')->middleware('auth:web,admin');


