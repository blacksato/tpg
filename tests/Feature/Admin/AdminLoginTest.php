<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminLoginTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function login_in_as_an_admin_inactive()
    {
        $this->withoutExceptionHandling();
        $email = 'sato@admin.net';
        $password = 'qazwsx123';
        $admin = $this->createAdmin([
             'email' => $email,
             'password' => bcrypt($password),
             'status' => 'A',
         ]);
        $this->post('core-admin/login', compact('email', 'password'))->assertRedirect('core-admin');
        $this->assertAuthenticatedAs($admin, 'admin');
    }

    /** @test */
    public function login_in_as_an_admin()
    {
        $this->withoutExceptionHandling();
        $email = 'sato@admin.net';
        $password = 'qazwsx123';
        $admin = $this->createAdmin([
            'email' => $email,
            'password' => bcrypt($password),
        ]);
        $this->post('core-admin/login', compact('email', 'password'))
            ->assertRedirect('core-admin');
        $this->assertAuthenticatedAs($admin, 'admin');
    }

    /** @test */
    public function cannot_login_with_invalid_credentials()
    {
        $this->withExceptionHandling();
        $email = 'sato@admin.net';
        $password = 'qazwsx123';
        $admin = $this->createAdmin([
            'email' => $email,
            'password' => bcrypt($password),
        ]);
        $this->post('core-admin/login', ['email' => $email, 'password' => '123456789'])
            ->assertStatus(302)
            ->assertSessionHasErrors([
                'email' => __('auth.failed'),
            ]);
        $this->assertGuest();
    }

    /** @test */
    public function cannot_login_with_user_credentials()
    {
        $this->withExceptionHandling();
        $email = 'user@styde.net';
        $password = 'laravel';
        $this->createUser([
            'email' => $email,
            'password' => bcrypt($password),
        ]);
        $this->post('core-admin/login', compact('email', 'password'))
            ->assertStatus(302)
            ->assertSessionHasErrors([
                'email' => __('auth.failed'),
            ]);
        $this->assertGuest();
    }
}
