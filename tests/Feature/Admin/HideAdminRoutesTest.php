<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;

class HideAdminRoutesTest extends TestCase
{
    /** @test */
    public function it_does_not_allow_guest_to_discovered_admin_urls()
    {
        $this->get('core-admin/invalid-url')
            ->assertStatus(302)
            ->assertRedirect('core-admin/login');
    }

    /** @test */
    public function it_does_not_allow_guest_to_discovered_admin_urls_using_post()
    {
        $this->post('core-admin/invalid-url')
            ->assertStatus(302)
            ->assertRedirect('core-admin/login');
    }

    /** @test */
    public function it_display_404s_when_admins_visit_invalid_urls()
    {
        $this->actingAsAdmin()
            ->get('core-admin/invalid-url')
            ->assertStatus(404);
    }
}
