<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminDashboardTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admins_can_visit_the_admin_dashboard()
    {
        $this->withoutExceptionHandling();
        $this->actingAsAdmin()
            ->get(route('admin_dashboard'))
            ->assertSee('Admin Panel')
            ->assertStatus(200);
    }

    /** @test */
    public function non_admin_users_cannot_visit_the_admin_dashboard()
    {
        $this->actingAsUser()
            ->get(route('admin_dashboard'))
            ->assertStatus(302)
            ->assertRedirect('core-admin/login');
    }

    /** @test */
    public function guest_cannot_visit_the_admin_dashboard()
    {
        $this->get(route('admin_dashboard'))
            ->assertStatus(302)->assertRedirect('core-admin/login');
    }
}
