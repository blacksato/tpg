<?php

namespace App\Http\Controllers\Admin;

use App\Core\Eloquent\Slide;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SlideRequest;
use Storage;

class SlideController extends Controller
{
    public function __construct()
    {
        $this->abilityCRUD('slide');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.slides.index')->with(['filter' => $request->filter,
         'slides' => Slide::url($request->filter)->orderBy('position', 'desc')->paginate(5), ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slides.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param App\Http\Requests\Admin\SlideRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SlideRequest $request)
    {
        Slide::create($request->validated());
        session()->flash('status', __('Proccess OK'));

        return redirect()->route('slides.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Core\Eloquent\Slide $slide
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Slide $slide)
    {
        return view('admin.slides.edit', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Core\Eloquent\Slide $slide
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SlideRequest $request, Slide $slide)
    {
        $slide->fill($request->validated());
        $slide->save();
        session()->flash('status', __('Proccess OK'));

        return redirect()->route('slides.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Core\Eloquent\Slide $slide
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slide $slide)
    {
        $slide->delete();
        session()->flash('status', __('Proccess OK'));

        return redirect()->route('slides.index');
    }
}
