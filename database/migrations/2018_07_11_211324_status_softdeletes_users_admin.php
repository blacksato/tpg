<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StatusSoftdeletesUsersAdmin extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('status', 1)->nullable();
            $table->string('avatar', 250)->nullable();
            $table->softDeletes();
        });
        Schema::table('admins', function (Blueprint $table) {
            $table->string('status', 1)->nullable();
            $table->string('avatar', 250)->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('admins');
        Schema::dropIfExists('users');
    }
}
