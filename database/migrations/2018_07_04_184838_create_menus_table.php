<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150);
            $table->string('route', 150)->nullable();
            $table->string('icon', 50)->nullable();
            $table->unsignedInteger('parent')->nullable();
            $table->smallInteger('order')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('parent')->references('id')->on('menus');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
